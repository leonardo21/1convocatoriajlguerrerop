﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using System.IO;

namespace 1Convocatoriajlguerrerop
{
    /// <summary>
    /// Lógica de interacción para Pedido.xaml
    /// </summary>
    public partial class Pedido : Window
    {

        public Pedido()
        {
            InitializeComponent();
            btnNuevo.IsEnabled = true; btnGuardarDatos.IsEnabled = false; btnSeleccionar.IsEnabled = false;
            cboxListProductos.IsEnabled = false;
            txtnumPedido.IsEnabled = false;
            txtCliente.IsEnabled = false;
            txtNomLista.IsEnabled = false;
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            btnGuardarDatos.IsEnabled = false; btnCrearLista.IsEnabled = true; btnNuevo.IsEnabled = false;
            btnSeleccionar.IsEnabled = false; btnNuevo.IsEnabled = false; txtCliente.IsEnabled = false;
            txtNomLista.IsEnabled = true;
            txtNomLista.Text = "";
            l = 0;
        }

        public void CrearListaXml(string NomLista) //Crear Lista
        {
            bandera++;
            doc = new XmlDocument();
            nom = doc.CreateElement("Lista" + NomLista);
            doc.AppendChild(nom);

            ruta = "C:\\Examen\\" + bandera + ".Lista" + NomLista + ".xml";
            arbolito.Items.Add(ruta);
            doc.Save("C:\\Examen\\" + bandera + ".Lista" + NomLista + ".xml");
        }

        private void btnCrearLista_Click(object sender, RoutedEventArgs e)
        {
            btnGuardarDatos.IsEnabled = true; btnCrearLista.IsEnabled = false; btnNuevo.IsEnabled = true;
            btnSeleccionar.IsEnabled = true; btnNuevo.IsEnabled = true; txtCliente.IsEnabled = true;
            txtNomLista.IsEnabled = false;

            NomLista = txtNomLista.Text;
            CrearListaXml(NomLista);
        }

        private void btnGuardarDatos_Click(object sender, RoutedEventArgs e)
        {
            

            Lista = new string[Ele];
            if (l < Ele)
            {
                for (int i = 0; i < Lista.Length; i++)
                {
                    if (Lista[i] == txtCliente.Text)
                    {
                        d = false;
                        MessageBox.Show("false");
                    }
                    else
                    {
                        d = true;
                    }
                }

                if (d == true)
                {
                    Console.WriteLine(txtCliente.Text);
                    Lista[l] = txtCliente.Text;
                    NomLista = txtNomLista.Text;
                    AgregarElemento(txtCliente.Text, NomLista);
                    l++;
                }
                else
                    MessageBox.Show("Item Duplicado");
            }
            else
                MessageBox.Show("Limites de Items alcanzado");

            txtCliente.Text = "";
            d = false;
        }

        public void AgregarElemento(string Elemento, string NomLista) //Agregar Elemento
        {
            nom.AppendChild(doc.CreateTextNode("\n" + Elemento));
            doc.Save("C:\\Examen\\" + bandera + ".Lista" + NomLista + ".xml");
            MessageBox.Show("Ingresado Correctamente");
        }

        private void btnSeleccionar_Click(object sender, RoutedEventArgs e)
        {
            cuadroTexto.Text = "";
            XmlTextReader xmlTextReader = new XmlTextReader(arbolito.SelectedItem.ToString());
            string Ultimaetiqueta = "";

            while (xmlTextReader.Read())
            {
                if (xmlTextReader.NodeType == XmlNodeType.Element)
                {
                    cuadroTexto.Text += (new string(' ', xmlTextReader.Depth * 3) + "<" + xmlTextReader.Name + ">");
                    Ultimaetiqueta = xmlTextReader.Name;
                    continue;
                }
                if (xmlTextReader.NodeType == XmlNodeType.Text)
                {
                    cuadroTexto.Text += xmlTextReader.ReadContentAsString() + "</" + Ultimaetiqueta + ">";
                }
                else
                    cuadroTexto.Text += ("\r");
            }

          
        }
        XmlDocument doc;
        XmlElement nom;
        int bandera = 0, Ele = 10000000, l = 0;
        string NomLista, ruta;
        string[] Lista;
        Boolean d = false;
    }
       
    
}
